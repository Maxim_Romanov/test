from homework4lesson import song


file = open("file.txt", "w")
file.write(song(9, 11, 0))

file.close()


file = open("lesson5.py", "r", encoding='utf8')
print(file.read())
# print(file.readlines(20))

file.close()


input_file = open("file.txt", encoding='utf8')

for line in input_file:
    print(line.rstrip() + '!')

input_file.close()


if __name__ == "__main__":
    pass
