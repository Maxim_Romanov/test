# Есть список строк. Посчитать сколько строк с нечётной длинной

lst = ['asd', 'asd1', 'asd2', 'wer3', 'hfg4', 'hfg5', 'wer']
y = 0
for i in lst:
    if len(i) % 2 != 0:
        y += 1

print(y)


# Есть словарь. Вывести на экран "ключ: значение" каждую в новой строке.
# Вывести на экран тип значений

d = {'name': 'name', 'iterations': 10, 'key1': 135}
for key in d:
    print(key, ':', d[key])
    print(type(d[key]))


def func(*args):
    '''
    создаёт функцию, которая принимает много разных аргументов. Собирает их в кортеж
    '''
    return args


print(func(1, 2, 5, 'aas'))


def func(*args):
    args = list(args)
    args.sort()
    return args[1]


print(func(1, 2, 5, 'adsf'))


def func(*args):
    args = sorted(args)
    return args[0]


x = func(1, 2, 5, 'adsf')
print(x)


def func(**kwargs):
    return kwargs


x = func(a=1, job='work', name='Sven')
print(x)

l = ['Vasia', 'Sonia', 'Max']
print(*l)


names = ['vasya', 'Sasha', 'olena', '123123', 'KOLYA']

names = [name.title() for name in names if name.isalpha()]
names.sort()
print(names)

names = ['vasya', 'Sasha', 'olena', '123123', 'KOLYA']

d = {i: len(i) for i in names}
print(d)

# Проверить, не работает
dict_abc = {'a': 1, 'b': 2, 'c': 3, 'd': 3}
dict_123 = {v: k for k, v in dict_abc.item()}
print(dict_123)


d = {"a": "V", "b": "W", "age": 34}
# d = {d[k]: for f in d}
d2 = {k: d[k].lower() for k in d if type(d[k]) == str}

print(d2)


names = ['vasya', 'Sasha', 'olena', '123123', 'KOLYA']

names = (name.title() for name in names if name.isalpha())

print(names)


names = ['vasya', 'Sasha', 'olena', '123123', 'KOLYA']

names = (name.title() for name in names if name.isalpha())

# print(next(names))
# print(next(names))
# print(next(names))

for i in names:
    print(i)
