from lesson5 import Person
import datetime

# Задание 1 (на создание классов)
# Переделываем (а что-то повторяем и закрепляем) наши классы таким образом:
# 1) Person (два свойства: 1. теперь full_name пусть будет свойством а не функцией
# (одно поле, мы ожидаем - тип строка и состоит из двух слов «имя фамилия»),
# а свойств name и surname нету, 2. год рождения).
# Реализовать методы, которые:
# 1. выделяет только имя из full_name
# 2. выделяет только фамилию из full_name;
# 3. вычисляет сколько лет есть/исполнится в году, который передаётся параметром (obj.age_in(year));
# если не передавать параметр, по умолчанию, сколько лет в этом году;
# ** (только для продвинутых) Можете в конструкторе проверить, что в full_name передаётся строка,
# состоящая из двух слов, если нет, вызывайте исключение 😊
# ** (только для продвинутых) Можете в конструкторе проверить, что в год рождения меньше 2019,
# но больше 1900, если нет вызывайте исключение


class PersonNew(Person):
    """
    reworked class
    """
    def __init__(self, full_name='', age=None):
        self.full_name = full_name
        self.age = age

    def name(self):
        nm = self.full_name.split(' ')
        return nm[0]

    def surname(self):
        snm = self.full_name.split(' ')
        return snm[1]

    def age_in(self, year=None):
        now = datetime.datetime.now()
        if year is None:
            return self.age
        else:
            return year - (now.year - int(self.age))


# 2) Employee (наследуемся от Person) (добавляются свойства: должность, опыт работы, зарплата)
# ** (только для продвинутых) Можете в конструкторе проверить, что в опыт работы и зарплата не отрицательные 😊
# Реализовать новые методы:
# 1. возвращает должность с приставкой, которая зависит от опыта работы: Junior — менее 3 лет,
# Middle — от 3 до 6 лет, Senior — больше 6 лет.
# Т.е. метод должен вернуть позицию с приставкой Junior/Middle/Senior <position>.
# Если, например у вас объект имел должность “programmer”  с опытом 2 года, метод должен вернуть “Junior programmer”
# 2. метод, который увеличивает зарплату на сумму, которую вы передаёте аргументом.


class Employee(Person):
    def __init__(self, position, exp, salary, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.position = position
        self.exp = exp
        self.salary = salary

    def exp_level(self):
        if self.exp <= 3:
            return f'Junior {self.position}'
        elif 3 < self.exp < 6:
            return f'Middle {self.position}'
        else:
            return f'Senior {self.position}'

    def growth_salary(self, slr_gr):
        self.salary = self.salary + slr_gr
        return self.salary


# 3) ITEmployee (наследуемся от Employee)
# 1. Реализовать метод добавления одного навыка в новое свойство skills (список)
# новым методом add_skill (см. презентацию).
# 2. * Реализовать метод добавления нескольких навыков в новое свойство skills (список) новым методом add_skills.
# Тут можно выбрать разные подходы: или аргумент один и он список навыков, которым вы расширяете список-свойство skill,
# или вы принимаете неопределённое количество аргументов, и все их добавляете в список-свойство skill

class ITEmployee(Employee):
    def __init__(self, skills, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.skills = []

    def add_skills(self, skill):
        self.skills.append(skill)


# Задание 2 (на создание новых классов)
# Создать классы
# 1) Прямоугольная площадка (пример: комната) (свойства: две стороны).
# Методы:
# 1. вычисляем площадь,
# 2. вычисляем периметр.


class Room:
    def __init__(self, line1, line2):
        self.line1 = line1
        self.line2 = line2

    def perimeter(self):
        return (int(self.line1) * 2) + (int(self.line2) * 2)

    def square(self):
        return int(self.line1) * int(self.line2)

# 2) Точка на карте (свойства: X, Y).
# Методы:
# 1. Нужно вычислить расстояние до начала координат,
# 2. * вычисляется расстояние между точкой и другой точкой экземпляром этого же класса
# 3. ** перевод в другие системы координат


class PointInTheMap:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def point_to_zero(self):
        dist = ((0 - self.x) ** 2 + (0 - self.y) ** 2) ** 0.5
        return dist

    def point_to_point(self, point):
        """
        :param x: x of another exemplar of this class
        :param y: y of another exemplar of this class
        :return: distance between two points - exemplars of this class
        """
        dist = ((point.x - self.x) ** 2 + (point.y - self.y) ** 2) ** 0.5
        return dist


if __name__ == "__main__":
    xx = PersonNew('Max Romanov', 33)
    print(xx.name())
    print(xx.surname())
    print(xx.age_in(2030))

    yy = Employee(name='Max', surname='Romanov', age=33, position='QA', exp=5, salary=1000)
    print(yy.exp_level())
    print(yy.growth_salary(500))

    rr = ITEmployee(name='Max', surname='Romanov', age=33, position='QA', exp=5, salary=1000)
    rr.add_skills('testing')
    print(rr)

    zz = Room(10, 15)
    print(zz.perimeter())
    print(zz.square())

    qq = PointInTheMap(1, 1)
    print(qq.point_to_zero())

    ww = PointInTheMap(5, 1)
    print(ww.point_to_point(qq))
