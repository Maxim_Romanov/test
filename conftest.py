import pytest
import requests


@pytest.fixture(scope='session')
def base_url():
    return "http://pulse-rest-testing.herokuapp.com"
    # book_id = None


book_list = [
            {"title": "Train who can", "author": "Major Pain"},
            {"title": "!@#$%^&*(", "author": "Major Pain"},
            {"title": "Train who can", "author": "123123"}
            ]


@pytest.fixture(params=book_list, ids=[str(x) for x in book_list])
def book(base_url, request):
    book_data = request.param
    yield book_data
    if "id" in book_data:
        requests.delete(f'{base_url}/books/{book_data["id"]}')


# @pytest.fixture()
# def wrong_book(base_url):
#     book_data = {"title": "",
#                  "author": "Title"}
#     return book_data
