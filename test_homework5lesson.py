import unittest
from homework3lesson import is_year_leap, fig, fig_up
from homework5lesson import Employee


# Задание 3 (на создание тестов c помощью unittest)
#
# Создайте наборы тестов на написанные функции из прошлого домашнего задания:
# Написать функцию is_year_leap, принимающую 1 аргумент — год, и возвращающую True, если год високосный, и False иначе.
# Функция принимает три числа a, b, c. Функция должна определить, существует ли треугольник с такими сторонами.
# Если треугольник существует, вернёт True, иначе False.
# Функция принимает три числа a, b, c. Функция должна определить, существует ли треугольник с
# такими сторонами и если существует, то возвращает тип треугольника Equilateral triangle (равносторонний),
# Isosceles triangle (равнобедренный), Versatile triangle (разносторонний) или не треугольник (Not a triangle).


class TestIsYearLeap(unittest.TestCase):
    def test_is_true(self):
        res = is_year_leap(2008)
        self.assertEqual(res, True)

    def test_is_false(self):
        res = is_year_leap(2007)
        self.assertEqual(res, False)


class TestFig(unittest.TestCase):
    def test_is_true(self):
        res = fig(3, 4, 5)
        self.assertEqual(res, True)

    def test_is_false(self):
        res = fig(1, 10, 1)
        self.assertEqual(res, False)


class TestFigUp(unittest.TestCase):
    def test_is_not(self):
        res = fig_up(1, 10, 100)
        self.assertEqual(res, 'Not a triangle')

    def test_is_equilateral(self):
        res = fig_up(5, 5, 5)
        self.assertEqual(res, 'Equilateral triangle (равносторонний)')

    def test_is_isosceles(self):
        res = fig_up(5, 5, 7)
        self.assertEqual(res, 'Isosceles triangle (равнобедренный)')

    def test_is_versatile(self):
        res = fig_up(3, 4, 5)
        self.assertEqual(res, 'Versatile triangle (разносторонний)')


# * Задание 4 (на создание тестов c помощью unittest)
# Создайте наборы тестов на тестирование класса ITEmployee, который вы реализовали в Задании 1
# (или Employee, или Person в зависимости до куда вы дошли в выполнении Задания 1).


# class TestEmployee(unittest.TestCase):
#     def test_is_junior(self):
#         res = Employee(exp_level(age=33, position='QA', exp=2, salary=1000))
#         self.assertEqual(res, 'Junior QA')
