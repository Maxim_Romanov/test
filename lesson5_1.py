from lesson5 import Person


class Employee(Person):
    """
    get some new
    """
    def __init__(self, name='', surname='', age=None, position='', salary=0):
        Person.__init__(self, name, surname, age)
        # super().__init__(name, surname, age) # it work too
        self.position = position
        self.salary = salary

    # def __init__(self, *args, position='', salary=0, **kwargs):
    #     Person.__init__(self, *args, **kwargs)
    #     self.position = position
    #     self.salary = salary

    def __str__(self):
        old_str = super().__str__()
        result = old_str.replace('Person', 'Employee', 1)
        return result
        # return f'<Employee object with name = {self.name}, surname = {self.surname}, age = {self.age}, ' \
        #     f'position = {self.position} and salary = {self.salary}>'

    def income(self, mounts=1):
        """
        which person have money from fixed time
        :param mounts: time period
        :return: summ of money which person have from {mounts}
        """
        return self.salary * mounts


class ITEmployee(Employee):
    """
    add skills to Employees
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.skills = []

    def add_skill(self, new_skill):
        self.skills.append(new_skill)

    def __str__(self):
        return f'<Employee object with name = {self.name}, surname = {self.surname}, age = {self.age}, ' \
            f'position = {self.position}, salary = {self.salary} and skills: {self.skills}>'


if __name__ == "__main__":
    # e1 = Employee(salary=100)
    # print(e1.income(10))
    # print(e1)
    e2 = ITEmployee('Victor', 'Volkov', 33, salary=100, position='tester')
    print(e2)
    e2.add_skill(new_skill='testing')
    e2.add_skill(new_skill='Python')
    print(e2)
