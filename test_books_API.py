import unittest
import requests


class FindBook(unittest.TestCase):

    def setUp(self):
        self.url = "http://pulse-rest-testing.herokuapp.com"
        self.book_id = None

    def test_book_create(self):
        book_data = {"title": "Train who can", "author": "Major Pain"}
        book = requests.post(f"{self.url}/books", data=book_data)
        self.assertEqual(201, book.status_code)
        body = book.json()
        self.book_id = body["id"]

        # 1 BAD
        # self.assertEqual(book_data.get("title"), body.get("title"))

        # 2 GOOD
        for key in book_data:
            self.assertEqual(book_data.get(key), body.get(key))

        # 3 GOOD too, maybe BEST
        book_data["id"] = self.book_id
        print(body, book_data)
        self.assertEqual(body, book_data)

        print(self.book_id)

    def tearDown(self):
        if self.book_id is not None:
            requests.delete(f"{self.url}/books/{self.book_id}")

# jsonschema - модуль для сложного жсона, когда надо проверить схему жсона
