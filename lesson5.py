# # lesson 4 notes
# x = int()
# l = list()
# s = str()
#
# print(type(x))
# print(type(l))
# print(type(s))
# # int, list, str - classes, x, l, s - empty data
# # function dir() help to look all attributes


# class Person:
#     def printer_self(self):
#         print(self)
#
#
# if __name__ == "__main__":
#     p1 = Person()
#     print("p1: ", p1)
#     p1.printer_self()
#
#     p2 = Person()
#     print("p2: ", p2)
#     p2.printer_self()


class Person:
    title = 'All people'

    def printer_self(self):
        print(self)

    def __init__(self, name='', surname='', age=None):
        self.name = name
        self.surname = surname
        self.age = age

    def __str__(self):
        return f'<Person object with name = {self.name}, surname = {self.surname} and age = {self.age}>'

    def full_name(self):
        """
        :return: name + surname
        """
        return f'{self.name} {self.surname}'

    def get_older(self, years=1):
        """
        get Person older for {years} adn rewrite it
        :return: None
        """
        if self.age is None:
            self.age = years
        else:
            self.age += years


if __name__ == "__main__":
    p1 = Person('Victor', 'Volkov', 33)
    p2 = Person('Vova')
    print(p1.name, p1.surname, p1.age)
    print(p1.title)

    print(p2.name, p2.surname, p2.age)
    print(p2.title)

    print(p1.title is p2.title)
    print(p1)
    print(p1.full_name())
    print(p1.get_older(11))
    print(p1)
