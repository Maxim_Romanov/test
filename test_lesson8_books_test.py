import requests
import pytest


# lesson 8
# pyTest


def test_book_create(base_url, book):

    response = requests.post(f"{base_url}/books", data=book)

    assert 201 == response.status_code

    body = response.json()
    book["id"] = body["id"]

    assert body == book


wrong_books_list = [
                    ({"title": "Title", "author": ""}, {"author": ['This field is required.']}),
                    ({"title": "", "author": "author"}, {"title": ['This field is required.']})
                    ]


# @pytest.mark.xfail(raises=(ValueError, AssertionError))
@pytest.mark.parametrize("wrong_book, expected_body", wrong_books_list, ids=["Empty author", "Empty title"])
def test_book_create_negative(base_url, wrong_book, expected_body):

    response = requests.post(f"{base_url}/books", data=wrong_books_list)
    # raise ValueError("Incorrect!")

    assert 400 == response.status_code

    body = response.json()

    # assert body == {'title': ['This field may not be blank.']}
    # assert ['This field may not be blank.'] in body.values()

    assert body == expected_body
