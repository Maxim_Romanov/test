print(123, 'sad', end=' ')
print('Hello', end=', ')
print('Hello!')


# срезы
s = "Hi there!"
print(s[0:-3:2])
print(s[::-1])
print(s[::-2])

print(s[1:-2:-1])
# nothing to print - Python can`t do it

# посмотреть трёхместное выражение if/else

x = 10
while x > 0:
    print(str(x) + '!')
    x -= 1


l = [1, 3, 5, 6, 7]
while l:
    l = l[0:-1]
    print(l)
print('Список закончился')


l = [1, 3, 5, 6, 7]
while l:
    print(l.pop())

for element in iterable_obj:
    expression

l = [1, 10, 3, 5, 6, 7, 11]
even = []
odd = []

for item in l:
    if item % 2 == 0:
        even.append(item)
    elif item % 2 == 1:
        odd.append(item)
    else:
        print(item, 'is not integer')

print(even)
print(odd)

l = [1, 10, 3, 5, 6, 7, 11]
for item in l:
    if item % 2 == 0:
        print(item)
    elif item % 2 == 1:
        print(item + 1)
    else:
        print(item, 'is not integer')


l = [1, 10, 3, 5, 6, 7, 11]
for item in range(len(l)):
    print('item = ', item)
    print('l[item]', l[item])
    # if item % 2 == 1:
    #     item = item + 1
#

l = [1, 10, 3, 5, 6, 7, 11]
for item in range(len(l)):
    if l[item] % 2 == 1:
        l[item] += 1

print(l)


while True:
    print('Type the world:')
    word = input('Your word: ')
    if ' ' not in word:
        break
    else:
        print('Enter word without space!')


while True:
    print('Type the number:')
    word = input('Your number: ')
    if word.isdigit():
        break
    else:
        print('Not a number!')

num = int(word)


try:
  print(10/0)
except ZeroDivisionError:
  print(unknovn_var)
finally:
  print('This is executed last')


while True:
    print('Type the number:')
    word = input('Your number: ')
    try:
        if float(word):
            word = float(word)
            break
        else:
            print('Write a number!')
    except (ValueError, TypeError):
        print('Enter the number again!')


while True:
    print('Type the number:')
    word = input('Your number: ')
    try:
        word = float(word)
        break
    except (ValueError, TypeError):
        print('Enter the number again!')


def repeat(s, exclaim):
    result = s * 3
    if exclaim:
        result = result + '!!!'
    return result


r = repeat('qwe', True)
print(r)


def func(x, y):
    gip = x**2 + y**2
    gip = gip**1/2
    return gip


g = func(4, 4)
print(g)
