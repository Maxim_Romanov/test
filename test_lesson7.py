import unittest
# from test_homework5lesson import TestFigUp
from homework3lesson import fig_up
import sys


print(sys.platform)

# lesson 7


class TestFigUp(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.something = "Set Up common"
        print(cls.something)

    def setUp(self):
        self.something2 = "SET Up"
        print(self.something2)

    def test_is_not(self):
        res = fig_up(1, 10, 100)
        self.assertEqual(res, 'Not a triangle')

    def test_is_equilateral(self):
        res = fig_up(5, 5, 5)
        self.assertEqual(res, 'Equilateral triangle (равносторонний)')

    @unittest.expectedFailure
    def test_is_equilateral_fail(self):
        res = fig_up(5, 10, 5)
        self.assertEqual(res, 'Equilateral triangle (равносторонний)')

    def test_is_isosceles(self):
        res = fig_up(5, 5, 7)
        self.assertEqual(res, 'Isosceles triangle (равнобедренный)')

    def test_is_versatile(self):
        res = fig_up(3, 4, 5)
        self.assertEqual(res, 'Versatile triangle (разносторонний)')

    def test_zero(self):
        """
        параметризация. Прогоняет один и тот же тест с несколькими значениями. Не валится после
        каждого фейла, доходит до конца
        :return:
        """
        number = [12, 5, 433, 0, 11, 6]
        for num in number:
            with self.subTest(num):
                print(num)
                res = num % 2
                self.assertEqual(res, 0)


if __name__ == "__main__":
    from HtmlTestRunner import HTMLTestRunner
    unittest.main(verbosity=2, testRunner=HTMLTestRunner(output=r'D:\Projects\QA_auto\test'))
    # # use "verbosity=2" for show table

    # test1 = TestFigUp('test_is_not')
    # test2 = TestFigUp('test_is_equilateral')
    # test3 = TestFigUp('test_is_isosceles')
    # test4 = TestFigUp('test_is_versatile')

    # print(test1.run())
    # print(test2.run())
    # print(test3.run())
    # print(test4.run())

    # suite1 = unittest.TestSuite([test1, test2, test3, test4])
    # suite1 = unittest.TestLoader().loadTestsFromTestCase(TestFigUp)
    # result = unittest.TestResult()
    # suite1.run(result)
    # print(result)

    # suite1.addTest(test1)
